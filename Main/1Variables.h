/// timers /// timers /// timers /// timers ///

uint32_t serialTimeOut = 0; // how long it has been since you recived input
uint32_t timer = 0;         // timer for imu

/// IMU /////// IMU /////// IMU /////// IMU ///

Kalman kalmanX; // Create the Kalman instances
Kalman kalmanY;
const int MPU_addr=0x68;    // I2C address of the MPU-6050
double accX, accY, accZ;    // the raw accelerometer values from imu
double gyroX, gyroY, gyroZ; // the raw gyroscope values from imu

/// Battery /// Battery /// Battery /// Battery ///

uint8_t sampleCount = 0;
float batteryTemp = 0;

//// motor //// motor //// motor //// motor ////

uint16_t throttle = 1000; // signal from controller to controll the drone's throttle
uint16_t esc1 = 0, esc2 = 0, esc3 = 0, esc4 = 0;


///////// PID ///////// PID ///////// PID ///////// PID /////////

// setpoint is the signal from the controller
// input is the signal from the imu/mpu6050
// output is the calculated value for the motors

double SetpointR, InputR, OutputR;  // for the Roll axis
double SetpointP, InputP, OutputP;  // for the pitch axis
double SetpointY, InputY, OutputY;  // for the yaw axis

float Kp_R = 2.00;                 // roll axis P parameter
float Ki_R = 0.00;                  // roll axis I parameter
float Kd_R = 0.00;                  // roll axis D parameter

float Kp_P = 2.00;                 // pitch axis P parameter
float Ki_P = 0.00;                  // pitch axis I parameter
float Kd_P = 0.00;                  // pitch axis D parameter

float Kp_Y = 2.00;                 // yaw axis P parameter
float Ki_Y = 0.00;                  // yaw axis I parameter
float Kd_Y = 0.00;                  // yaw axis D parameter

PID PID_R(&InputR, &OutputR, &SetpointR, Kp_R, Ki_R, Kd_R, DIRECT);
PID PID_P(&InputP, &OutputP, &SetpointP, Kp_P, Ki_P, Kd_P, DIRECT);
PID PID_Y(&InputY, &OutputY, &SetpointY, Kp_Y, Ki_Y, Kd_Y, DIRECT);

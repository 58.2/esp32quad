// serial input example 1020,0.2,20,2,1s
// now throttle = 1020, SetpointR = 0.2, SetpointP = 20, SetpointY = 2

void Setpoint(){
  while (Serial.available() > 0) {
    float   A = Serial.parseFloat();
    float   B = Serial.parseFloat();
    float   D = Serial.parseFloat();
    float   C = Serial.parseFloat();
    uint8_t E = Serial.parseInt();
    if (Serial.read() == 's') {
      serialTimeOut = millis();
      if (E == 1) {
        throttle  = int(A);
        SetpointR =     B;
        SetpointP =     C;
        SetpointY =     D;
      }
      else if (E == 2) { //changing PID variables
        if      (D == 1) Kp_R = A, Ki_R = B, Kd_R = C;
        else if (D == 2) Kp_P = A, Ki_P = B, Kd_P = C;
        else if (D == 3) Kp_Y = A, Ki_Y = B, Kd_Y = C;
      }
    }
  }
}



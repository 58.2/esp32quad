void PIDsetup() {
  PID_R.SetMode(AUTOMATIC);
  PID_P.SetMode(AUTOMATIC);
  PID_Y.SetMode(AUTOMATIC);
  
  PID_R.SetOutputLimits(-300, 300); // the lowest and highest output variaables
  PID_P.SetOutputLimits(-300, 300); // the lowest and highest output variaables
  PID_Y.SetOutputLimits(-100, 100); // the lowest and highest output variaables
  
  PID_Y.SetSampleTime(50); // how often it will update the output
  PID_P.SetSampleTime(50); // how often it will update the output
  PID_R.SetSampleTime(50); // how often it will update the output
}

void Pid() {
  PID_R.Compute(); //calculating the output from the P I and D values
  PID_P.Compute(); //calculating the output from the P I and D values
  PID_Y.Compute(); //calculating the output from the P I and D values
  
  uint16_t M1, M2, M3, M4;
  
  if (throttle >= 1150 && millis()/* - serialTimeOut < 150*/){
    //calculate the controller output and send it
    M1 = uint16_t(throttle-OutputR-OutputP+OutputY); //motor in bottom Left value
    M2 = uint16_t(throttle+OutputR-OutputP-OutputY); //motor in upper Left value
    M3 = uint16_t(throttle+OutputR+OutputP+OutputY); //motor in upper Right value
    M4 = uint16_t(throttle-OutputR+OutputP-OutputY); //motor in bottom Right value
    
    M1 = constrain(M1, 1100, 2000); // it limits the value between 1100 and 2000
    M2 = constrain(M2, 1100, 2000); // it limits the value between 1100 and 2000
    M3 = constrain(M3, 1100, 2000); // it limits the value between 1100 and 2000
    M4 = constrain(M4, 1100, 2000); // it limits the value between 1100 and 2000
    
    esc1 = M1;
    esc2 = M2;
    esc3 = M3;
    esc4 = M4;
  }
  else esc1 = 1000, esc2 = 1000, esc3 = 1000, esc4 = 1000;
}

/*
  
  Made by Daniel Fjeld
  Code for a brushless quadcopter
  Uses PID leveling
  Mpu6050 gyroscope
  HC_12 serial communication

  sourses:
  kalmanfilter: https://github.com/TKJElectronics/KalmanFilter
  wire return: http://arduino.cc/en/Reference/WireEndTransmission
  imu: http://www.freescale.com/files/sensors/doc/app_note/AN3461.pdf eq. 25 and eq. 26
  https://github.com/imxieyi/esp32-i2c-mpu6050


     front
  M2    .    M3
  \ \  /|\  / /
   \ \  |  / /
    \ \ _ / /
     |     |
     |  _  |
    / /   \ \
   / /     \ \
  / /       \ \
  M1         M4
  
  The parts can be found in parts.txt.
  Pictures and videos can be found in the media folder.
  The libreries can be found in the libreries folder.
*/
 ////////////////////////////////////////////////
#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_err.h"

#include "esp32-hal-ledc.h"
 ////////////////////////////////////////////////
#include <Arduino.h>   // lets you have multiple files
#include <Wire.h>      // library for the IMU
#include <PID_v1.h>    // PID algorithm
#include "Kalman.h"    // IMU algorithm
//#include "MPU6050.h"    // IMU algorithm
#include "1Variables.h" // file with all the variables

TaskHandle_t  Core0;

void setup() {
  ledcSetup(1, 50, 16); // channel 1, 50 Hz, 16-bit width
  ledcSetup(2, 50, 16); // channel 1, 50 Hz, 16-bit width
  ledcSetup(3, 50, 16); // channel 1, 50 Hz, 16-bit width
  ledcSetup(4, 50, 16); // channel 1, 50 Hz, 16-bit width
  ledcAttachPin(16, 1); // GPIO 18/esc1 assigned to channel 1
  ledcAttachPin(17, 2); // GPIO 18/esc1 assigned to channel 2
  ledcAttachPin(18, 3); // GPIO 18/esc1 assigned to channel 3
  ledcAttachPin(19, 4); // GPIO 18/esc1 assigned to channel 4
  pinMode(21,INPUT_PULLUP);
  pinMode(22,INPUT_PULLUP);
  Wire.begin(21, 22);; // sda and scl on pin 21 and 22
  Serial.begin(115200);
  xTaskCreatePinnedToCore(Output,"Output",1000,NULL,1,&Core0,1);
  delay(500);  // needed to start-up task
  
  delay (500);

  delay (500);
  IMUsetup();
  PIDsetup();
}

void loop() {
  Input();    // imu data
  Setpoint(); // serial data
  Pid();
  //debug();
}
